# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

from .htmlParser import HtmlParser


class HtmlParserSpringer(HtmlParser):
    text_type = None

    def __init__(self, soup):
        """

		:param soup: BeautifulSoup web page object
		"""
        super(HtmlParserSpringer, self).__init__(soup)
        self.get_text_type()

    def parse(self):
        if self.text_type == 'article':
            for tag in self.soup.find_all('meta'):
                self.get_journal_article(tag)
                self.get_publisher_article(tag)
                self.get_publication_date_article(tag)
                self.get_title_article(tag)
                self.get_author_article(tag)
                self.get_affiliation_article(tag)
                self.get_email_article(tag)

        elif self.text_type == 'book':
            self.get_journal_book()
            self.get_title_book()
            self.get_author_book()
            self.get_affiliation_book()
            self.get_email_book()

    def get_text_type(self):
        if self.soup.find(id='article') is not None:
            self.text_type = 'article'
        else:
            self.text_type = 'book'

    def get_title_book(self):
        for tags in self.soup.find_all(id='abstract-about-title'):
            self.title = tags.next_element.strip()
            # print(tags.next_element)

    def get_journal_book(self):
        for tags in self.soup.find_all(id='abstract-about-publisher'):
            # print(tags.next_element)
            self.publisher = tags.next_element.strip()

    def get_author_book(self):
        tags = self.soup.find_all('span', recursive=True, attrs={'class': re.compile(r'^[Aa]uthor[Nn]ame$')})
        for tag in tags:
            self.authors.append(tag.text.strip())

    def get_affiliation_book(self):
        tags = self.soup.find_all('span', recursive=True,
                                  attrs={'class': re.compile(r'^[Aa]uthor[s]?[Nn]ame_affiliation$')})
        for tag in tags:
            for child in tag.children:
                if child.name == 'span':
                    self.affiliations.append(child.text.strip())

    # def get_affiliation_book(self):
    #     for tags in self.soup.find_all(attrs={'class': 'affiliation'}):
    #         # print(tags)
    #         # print(tags.next_element.strip())
    #         try:
    #             self.affiliations.append(tags.next_element.strip())
    #         except TypeError as te:
    #             pass

    # def get_affiliation_book(self):
    #     for tags in self.soup.find_all(attrs={'name' : re.compile(r'^citation_author_institution$')}):
    #         if tags['content'].strip() not in self.affiliations:
    #             self.affiliations.append(tags['content'].strip())

    def get_email_article(self, tag):
        try:
            if 'citation_author_email' == tag['name']:
                self.emails.append(tag['content'])
        except BaseException as e:
            pass
            # print('Exception found! {0}'.format(str(e)))

    def get_email_book(self):
        for tags in self.soup.find_all(attrs={'class': 'envelope'}):
            self.emails.append(tags['title'].strip())
