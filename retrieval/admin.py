# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# from .models import Author, Article, Affiliation

# Register your models here.
# class ArticleInline(admin.StackedInline):
#     model = Article
#
#
# class ArticleAdmin(admin.ModelAdmin):
#     fieldsets = [
#         (None, {'fields': ['Publisher', 'Journal', 'Title', 'Publication Date']}),
#     ]
#
#
# class AuthorAdmin(admin.ModelAdmin):
#     fieldsets = [
#         (None,                  {'fields': ['Name']}),
#         ('Author information',  {'fields': ['Email']}),
#     ]
#     # inlines = [ArticleInline]
#
#
# class AffiliationAdmin(admin.ModelAdmin):
#     fieldsets = [
#         (None, {'fields': ['Name', 'Location']}),
#     ]
#
# admin.site.register(Author, AuthorAdmin)
# admin.site.register(Article, ArticleAdmin)
# admin.site.register(Affiliation, AffiliationAdmin)
