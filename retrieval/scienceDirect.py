# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from bs4 import Tag

from .htmlParser import HtmlParser


class HtmlParserScienceDirect(HtmlParser):
    def __init__(self, soup):
        super(HtmlParserScienceDirect, self).__init__(soup)
        self.auth_relation = dict()

    def relate_affiliations(self, v):
        if re.match(r'^[a-z]$', v):
            return self.affiliations[ord(v) - 97]
        else:
            return self.affiliations[0]

    def parse(self):
        tag = self.soup.find(attrs={'class': 'svTitle'})
        self.title = tag.text.strip()

        i = 0
        for section in self.soup.find_all('a', attrs={'class': 'svAuthor'}):
            self.authors.append(section.text.strip())
            next_node = section

            while next_node is not None:
                next_node = next_node.next_sibling
                try:
                    tag_name = next_node.name
                except AttributeError:
                    tag_name = str()

                if tag_name == 'a' and 'auth_aff' in next_node['class']:

                    try:
                        aff = re.search(r'^.+ ([a-z])$', next_node['title'], re.MULTILINE).group(1)
                    except AttributeError:
                        aff = str()

                    try:
                        if isinstance(self.auth_relation[i], list):
                            self.auth_relation[i].append(aff)
                        else:
                            last = self.auth_relation[i]
                            self.auth_relation[i] = [last, aff]
                    except KeyError:
                        self.auth_relation.update({i: aff})
            i += 1

        for tag in self.soup.find_all('li', id=re.compile(r'^af[a-z\d]*$')):
            for child in tag.children:
                if isinstance(child, Tag):
                    if child.name == 'span':
                        self.affiliations.append(child.text.strip())

        if len(self.auth_relation) > 0:
            # Now I have the letter relations and the affiliations, so let's configure them correctly
            for k, v in self.auth_relation.items():
                if isinstance(v, list):
                    aff = [self.relate_affiliations(value) for value in v]
                    self.auth_relation.update({k: aff})
                else:
                    self.auth_relation.update({k: self.relate_affiliations(v)})
        else:
            self.auth_relation.update({0: self.affiliations[0]})
