# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import tempfile
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.views import generic

from retrieval.forms import ArticleUrlForm, ArticleBatchForm
from .handlers import FileHandler, ArticleHandler
from .models import Article

handler = None


# Create your views here.
class ArticleFormView(generic.FormView):
    form_class = ArticleUrlForm
    template_name = 'retrieval/article_form.html'
    success_url = reverse_lazy('retrieval:search')

    def form_valid(self, form):
        data = form.cleaned_data
        super(ArticleFormView, self).form_valid(form)


class ResultsSingleView(generic.DetailView):
    model = Article
    template_name = 'retrieval/results_single.html'


class ArticleBatchView(generic.FormView):
    form_class = ArticleBatchForm
    template_name = 'retrieval/article_batch_form.html'
    success_url = reverse_lazy('retrieval:batch_handle')


def home(request):
    return render(request, 'retrieval/home.html')


def search(request):
    if request.method == 'POST':
        url = request.POST['url']
    else:
        url = ''
    if url == '':
        return HttpResponseRedirect(reverse_lazy('retrieval:results_single', args=(0,)))
    print(url)
    article = Article.objects.filter(url=url.lower())
    if article:
        return HttpResponseRedirect(reverse_lazy('retrieval:results_single', args=(article[0].pk,)))
    art_handler = ArticleHandler(url)
    article = art_handler.craft_article()
    return HttpResponseRedirect(reverse_lazy('retrieval:results_single', args=(article.pk,)))


def map_view(request):
    articles = request.session.get('articles')
    context = {'articles': articles}
    return render(request, 'retrieval/map.html', context)


def download_statistics(request):
    file_string = request.session.get('string')
    response = HttpResponse(file_string)
    response['Content-Disposition'] = 'attachment; filename="results.txt"'
    return response


def download_kml(request):
    file_kml = request.session.get('kml')
    response = HttpResponse(file_kml)
    response['Content-Disposition'] = 'attachment; filename="map.kml"'
    return response


def progress(request):
    if request.is_ajax():
        if handler is not None:
            perc = {'perc': handler.curr_percentage}
            return HttpResponse(json.dumps(perc), content_type='application/json')
        else:
            retn = {'err': None}
            return HttpResponse(json.dumps(retn), content_type='application/json')
    else:
        return HttpResponse()


def batch_handle(request):
    global handler
    handler = None
    file = tempfile.NamedTemporaryFile(delete=False)
    for line in request.FILES['file']:
        file.write(line)
    file_name = file.name
    file.close()
    handler = FileHandler(file_name)
    results = handler.craft_result()
    os.remove(file_name)
    request.session['string'] = results['string']  # Complete text generated for input sent
    request.session['articles'] = results['articles']  # List of articles, each with list of authors
    request.session['kml'] = results['kml']  # KML file generated
    return redirect(reverse_lazy('retrieval:map'))
