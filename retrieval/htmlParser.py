# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time

import re
from abc import ABCMeta, abstractmethod


class HtmlParser(metaclass=ABCMeta):
    def __init__(self, soup):
        self.soup = soup
        self.journal_title = None
        self.publisher = None
        self.title = None
        self.authors = list()
        self.auth_relation = None
        self.affiliations = list()
        self.emails = list()
        self.publication_date = None

    @abstractmethod
    def parse(self):
        pass

    def get_journal_article(self, tag):
        try:
            if 'citation_journal_title' in tag['name']:
                self.journal_title = tag['content'].strip()
        except BaseException as e:
            pass
            # print('Exception found! {0}'.format(str(e)))

    def get_author_article(self, tag):
        try:
            if 'citation_author' == tag['name']:
                if tag['content'] not in self.authors:
                    self.authors.append(tag['content'].strip())
        except BaseException as e:
            pass
            # print('Exception found! {0}'.format(str(e)))

    def get_affiliation_article(self, tag):
        try:
            if 'citation_author_institution' == tag['name']:
                self.affiliations.append(tag['content'].strip())
        except BaseException as e:
            pass
            # print('Exception found! {0}'.format(str(e)))

    def get_title_article(self, tag):
        try:
            if 'citation_title' in tag['name']:
                self.title = tag['content']
        except BaseException as e:
            pass
            # print('Exception found! {0}'.format(str(e)))

    def get_publisher_article(self, tag):
        try:
            if 'citation_publisher' in tag['name']:
                self.publisher = tag['content']
        except BaseException as e:
            # print(str(e))
            pass

    def get_publication_date_article(self, tag):
        try:
            if re.match(r'citation(_online)?_date', tag['name']) is not None:
                str_publication_date = tag['content']
                strct_date = time.strptime(str_publication_date, "%Y/%m/%d")
                self.publication_date = str(strct_date.tm_mday).zfill(2)
                self.publication_date += '/' + str(strct_date.tm_mon).zfill(2)
                self.publication_date += '/' + str(strct_date.tm_year)

        except KeyError as ke:
            # print(str(ke))
            pass
        except IndexError as ie:
            # print(str(ie))
            pass
        except ValueError as ve:
            # print(str(ve))
            pass
