# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

from .htmlParser import HtmlParser


class HtmlParserIOPScience(HtmlParser):
    def __init__(self, soup):
        super(HtmlParserIOPScience, self).__init__(soup)
        self.author_count = 0
        self.auth_relation = dict()

    def parse(self):
        self.get_authors()
        self.get_affiliations()
        self.relate_authors_affiliations()
        for tag in self.soup.find_all('meta'):
            self.get_journal_article(tag)
            self.get_publisher_article(tag)
            self.get_publication_date_article(tag)
            self.get_title_article(tag)

    def get_affiliations(self):
        for tag in self.soup.find_all('p', attrs={'class': 'mb-05'}):
            try:
                parent = tag.parent
                if 'wd-jnl-art-author-affiliations' in parent['class']:
                    if len(self.auth_relation) > 0:
                        self.affiliations.append(tag.text[2:])
                    else:
                        self.affiliations.append(tag.text)
            except AttributeError:
                pass
            except KeyError:
                pass

    def get_authors(self):
        tags = self.soup.find_all('span', attrs={'class': 'nowrap', 'itemprop': 'author'})
        for tag in tags:
            for desc in tag.children:
                try:
                    relations = re.findall(r'(\d+)', desc.text)
                    if len(relations) == 0:
                        self.authors.append(desc.text)
                    else:
                        relations = list(map(int, relations))
                        self.auth_relation.update({self.author_count: relations})
                except AttributeError:
                    pass
            self.author_count += 1

    def relate_authors_affiliations(self):
        global aff
        if len(self.auth_relation) > 0:
            for k, v in self.auth_relation.items():
                if len(v) == 1:
                    aff = self.affiliations[v[0] - 1]
                else:
                    aff = list()
                    for value in v:
                        if value - 1 >= len(v):
                            continue
                        aff.append(self.affiliations[value - 1])
                    if len(aff) == 1:
                        aff = aff[0]
                self.auth_relation.update({k: aff})
        else:
            for i in range(0, self.author_count):
                self.auth_relation.update({i: self.affiliations[0]})
