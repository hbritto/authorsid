# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import requests
import string
from bs4 import BeautifulSoup
from requests.exceptions import ConnectionError

from .acm import HtmlParserACM
from .ieee import HtmlParserIEEE
from .iopscience import HtmlParserIOPScience
from .physreve import HtmlParserPhysRevE
from .scienceDirect import HtmlParserScienceDirect
from .springer import HtmlParserSpringer
from .wiley import HtmlParserWiley


class HtmlInfo:
    def __init__(self, url=None):
        self.urlParsed = ""
        self.urlOrig = ""
        self.soup = ""
        self.letters = tuple(string.ascii_letters)
        if url is not None:
            self.feed(url)

    def feed(self, url):
        try:
            self.urlOrig = url
            req = requests.get(url)
            self.urlParsed = req.url
            self.soup = BeautifulSoup(req.text, 'lxml')
        except ConnectionError:
            self.urlParsed = None

    def springerUrlCorrection(self):
        url = self.urlParsed
        url = re.sub('%2f', '/', url)
        url += '?view=classic'
        self.feed(url)

    def get_magazine(self):
        if self.urlParsed is None:
            return None
        elif 'springeropen' in self.urlParsed:
            return None
        elif 'springer' in self.urlParsed:
            self.springerUrlCorrection()
            return HtmlParserSpringer(self.soup)
        elif 'ieee' in self.urlParsed:
            return HtmlParserIEEE(self.soup)
        elif 'iopscience' in self.urlParsed:
            return HtmlParserIOPScience(self.soup)
        elif 'acm' in self.urlParsed:
            return HtmlParserACM(self.soup)
        elif 'wiley' in self.urlParsed:
            return HtmlParserWiley(self.soup)
        elif 'sciencedirect' in self.urlParsed:
            return HtmlParserScienceDirect(self.soup)
        elif re.match(r'.*([Pp]hys[Rr]ev[Ee]).*', self.urlParsed) is not None:
            return HtmlParserPhysRevE(self.soup)
