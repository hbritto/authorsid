# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from _ssl import SSLError
from itertools import chain

import googlemaps
import pytz
import re
from datetime import datetime
from django.contrib.gis.geos import Point
from django.db import IntegrityError
from django.utils import timezone
from googlemaps.exceptions import ApiError, Timeout
from lxml import etree
from pykml.factory import KML_ElementMaker as KML

from .htmlInfo import HtmlInfo
from .models import Article, Author, Affiliation


class FileHandler:
    def __init__(self, file):
        self.curr_percentage = 0
        self.file = open(file)
        self.total = 0
        self.misses = 0
        self.hits = 0
        self.results = {'articles': list(), 'string': None, 'kml': None}
        self.article_handler = None
        self.article = None
        self.article_query = None
        self.url_begin = 'https://doi.org/'
        self.affiliation = None
        self.author_name = None
        self.docKml = KML.Document(id='id_document', name='Markers',
                                   description='Placemarkers for given set of articles')

    def add_to_kml(self):
        marker = KML.Placemark(KML.name(self.author_name),
                               KML.description(self.affiliation.name),
                               KML.Point(
                                   KML.coordinates(str(self.affiliation.location.x) + ',' +
                                                   str(self.affiliation.location.y)
                                                   )
                               )
                               )

        self.docKml.append(marker)

    def __string_article(self, article, authors, num_article):
        string = '{0}º: {1}\n\n'.format(str(num_article), article.title)
        aff_valid_list = []
        total = 0

        for ind, author in enumerate(article.authors.all()):
            self.author_name = author.name.strip()
            string += '{0}, '.format(self.author_name)

            self.affiliation = author.affiliation

            try:
                string += '{0}\n\n'.format(self.affiliation.name)

                aff_valid = self.affiliation.aff_valid

                author_dict = {'name': self.author_name, 'aff': self.affiliation.name,
                               'lat': 0.0, 'lng': 0.0, 'aff_valid': aff_valid}

                if aff_valid:
                    author_dict.update({'lat': self.affiliation.location.y})
                    author_dict.update({'lng': self.affiliation.location.x})
                    self.add_to_kml()
                    aff_valid_list.append(str(aff_valid))
                elif self.affiliation.name is not None or self.affiliation.name != '':
                    aff_valid_list.append('True')

            except AttributeError as ae:
                string += '\n\n'
                author_dict = {'name': self.author_name, 'aff': '',
                               'lat': 0.0, 'lng': 0.0, 'aff_valid': False}
                aff_valid_list.append('False')

            authors.append(author_dict)
            total = ind

        total += 1
        total_true = aff_valid_list.count('True')
        try:
            if (total_true / total) * 100 < 75:
                self.misses += 1
            else:
                self.hits += 1
        except ZeroDivisionError:
            self.misses += 1

        string += '\n\n'
        return string

    def count_articles(self):
        i = 0
        for i, l in enumerate(self.file, 1):
            pass
        self.file.seek(0)
        return i

    def craft_result(self):
        results_str = str()
        results_kml = b'<?xml version="1.0" encoding="UTF-8"?>'
        self.total = self.count_articles()
        for num_line, url in enumerate(self.file, 1):
            url = url.strip()
            self.curr_percentage = (num_line / self.total) * 100
            try:
                if re.match(r'http[s]?://', url) is None:
                    url = ''.join([self.url_begin, url])

                self.article_query = Article.objects.get(url=url.lower())
                authors = list()
                results_str += self.__string_article(self.article_query, authors, num_line)
                self.results['articles'].append(authors)

            except Article.MultipleObjectsReturned:
                self.article_query = Article.objects.filter(url=url.lower())
                authors = list()
                results_str += self.__string_article(self.article_query[0], authors, num_line)
                self.results['articles'].append(authors)

            except Article.DoesNotExist:
                try:
                    self.article_handler = ArticleHandler(url)
                    self.article = self.article_handler.craft_article()
                    authors = list()
                    results_str += self.__string_article(self.article, authors, num_line)
                    self.results['articles'].append(authors)

                except IntegrityError as ie:
                    results_str += '{0}º: {1}\n\n'.format(str(num_line), str(ie))
                    print(str(ie))
                    self.misses += 1

                except AttributeError as ae:
                    results_str += '{0}º: {1}\n\n'.format(str(num_line), str(ae))
                    print(str(ae))
                    self.misses += 1

                except IndexError as ie:
                    results_str += '{0}º: {1}\n\n'.format(str(num_line), str(ie))
                    print(str(ie))
                    self.misses += 1

                except TypeError as te:
                    results_str += '{0}º: {1}\n\n'.format(str(num_line), str(te))
                    print(str(te))
                    self.misses += 1

            except SSLError:
                print('SSLERROR!!! Article nº {0}'.format(num_line))

        percentage = (self.hits / self.total) * 100
        results_kml += etree.tostring(KML.kml(self.docKml))

        self.results['string'] = 'Total de artigos: {0}\n'.format(str(self.total))
        self.results['string'] += 'Total de acertos: {0}\n'.format(str(self.hits))
        self.results['string'] += 'Total de erros: {0}\n'.format(str(self.misses))
        self.results['string'] += 'Porcentagem de acertos: {0:.3f}%\n\n'.format(percentage)
        self.results['string'] += results_str
        self.results['kml'] = results_kml.decode('utf-8')
        return self.results


class ArticleHandler:
    def __init__(self, url):
        self.article = None
        self.pks = list()
        self.author = None
        self.info = None
        self.url = url
        self.magazine = None
        self.date_format = '%Y-%m-%d'
        self.affiliation = None

    def insert_affiliation(self, affiliation):
        aff = affiliation.strip()
        self.affiliation = Affiliation.objects.filter(name=aff)
        if self.affiliation:
            self.affiliation = self.affiliation[0]
            self.author.affiliation = self.affiliation
        else:
            self.affiliation = Affiliation()
            self.affiliation.name = aff
            self.locate_affiliation()
            self.affiliation.save()
            self.author.affiliation = self.affiliation

    # TODO: Test case didn't relate first author and affiliation
    def insert_authors(self):
        i = 0
        for auth in self.magazine.authors:
            self.author = Author.objects.filter(name=auth)
            if self.author:
                self.pks.append(self.author[0].pk)
            else:
                self.author = Author()
                self.author.name = auth.strip()

                # TODO: Correct dict value is list of list
                if isinstance(self.magazine.auth_relation, dict):
                    relation_length = len(self.magazine.auth_relation)
                    if relation_length > 1:
                        affiliations = None
                        if any(isinstance(i, list) for i in self.magazine.auth_relation):
                            affiliations = list(
                                chain.from_iterable(self.magazine.auth_relation))  # Perhaps the error is here
                        else:
                            affiliations = self.magazine.auth_relation
                        self.insert_affiliation(affiliations[i])

                    else:
                        self.insert_affiliation(self.magazine.auth_relation[0])

                elif isinstance(self.magazine.affiliations, list):
                    if any(isinstance(i, list) for i in self.magazine.affiliations):
                        affiliations = list(chain.from_iterable(self.magazine.affiliations))
                    else:
                        affiliations = self.magazine.affiliations

                    if len(affiliations) > 0:
                        self.insert_affiliation(affiliations[i])
                    else:
                        self.author.affiliation = None

                self.author.save()
                self.pks.append(self.author.pk)

            i += 1

    def craft_article(self):
        self.article = Article()
        self.info = HtmlInfo(self.url)
        self.magazine = self.info.get_magazine()
        self.magazine.parse()
        self.article.url = self.info.urlOrig.lower()
        self.article.title = self.magazine.title
        self.article.journal = self.magazine.journal_title
        self.article.publisher = self.magazine.publisher
        self.article.publication_date = self.magazine.publication_date
        self.article.save()
        self.insert_authors()
        for pk in self.pks:
            self.article.authors.add(Author.objects.get(pk=pk))
        return self.article

    def geolocate(self):
        try:
            gmap = googlemaps.Client(key='AIzaSyAtPpq0fC2ylRRKkZPuzIGmbOaut-X63xQ')
            geocode_results = gmap.geocode(self.affiliation.name)
            location = geocode_results[0]['geometry']['location']
            lnglat = Point(x=location['lng'], y=location['lat'])
            return lnglat
        except IndexError as ie:
            print(str(ie))
            return None
        except ApiError as ae:
            print(str(ae))
            return None
        except Timeout:
            print('gmaps timeout')
            return None

    def locate_affiliation(self):
        now = timezone.now()  # Tz Aware
        insertion_date = self.affiliation.insertion_date  # Should be tz aware
        age = 0
        if isinstance(insertion_date, str):
            insertion_date = datetime.strptime(insertion_date, self.date_format)
        if insertion_date is not None:
            insertion_date.replace(tzinfo=pytz.utc)
            age = (now - insertion_date).days

        if insertion_date is None or self.affiliation.aff_valid == False or age > 30:
            latlng = self.geolocate()
            if latlng is not None:
                self.affiliation.location = latlng
                self.affiliation.insertion_date = timezone.now()
                self.affiliation.aff_valid = True
            else:
                self.affiliation.location = None
                self.affiliation.aff_valid = False
