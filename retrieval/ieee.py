# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .htmlParser import HtmlParser


class HtmlParserIEEE(HtmlParser):
    text_type = None

    def __init__(self, soup):
        super(HtmlParserIEEE, self).__init__(soup)

    def parse(self):
        for tag in self.soup.find_all('meta'):
            self.get_journal_article(tag)
            self.get_publisher_article(tag)
            self.get_publication_date_article(tag)
            self.get_author_article(tag)
            self.get_affiliation_article(tag)
        self.get_title_article()

    def get_title_article(self, tag=None):
        for tag in self.soup.find_all('h1'):
            try:
                self.title = tag.text.strip()
            except BaseException as e:
                # print(str(e))
                pass

    def get_affiliation_article(self, tag):
        try:
            if 'citation_author_institution' in tag['name']:
                self.affiliations.append(tag['content'])
            else:
                size_af = len(self.affiliations)
                size_auth = len(self.authors)
                if size_auth > size_af > 0:
                    self.affiliations.append(self.affiliations[-1])
        except BaseException as e:
            # print(str(e))
            pass

    def get_publication_date_article(self, tag):
        try:
            if 'citation_date' in tag['name']:
                self.publication_date = tag['content']
        except KeyError as ke:
            # print(str(ke))
            pass

    def get_text_type(self):
        if self.soup.find({'name': 'citation_journal_title'}):
            self.text_type = 'article'
        elif self.soup.find({'name': 'citation_book'}):
            self.text_type = 'book'
