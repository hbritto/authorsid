# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.urlresolvers import reverse
from django.test import TestCase

from .models import Article


class HomePageTest(TestCase):
    def test_index_is_redirecting_correctly(self):
        response = self.client.get(reverse('index'), follow=True)
        self.assertRedirects(response, reverse('retrieval:article_input'))


class ArticleTest(TestCase):
    def test_article_is_saved_correctly(self):
        first_article = Article.objects.create()
        second_article = Article()
        second_article.save()

        self.assertEqual(Article.objects.count(), 2)


class ArticleFormTest(TestCase):
    def test_form_is_redirecting_after_POST(self):
        response = self.client.post(reverse('retrieval:search'), {'url': ''}, follow=False)
        self.assertEqual(response.status_code, 302)


class SearchTest(TestCase):
    def _test_search_is_getting_article(self):
        response = self.client.post(reverse('retrieval:search'),
                                    {'url': 'http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6919256'},
                                    follow=True)
        article = Article.objects.first()
        self.assertEqual(article.title, 'Information Security in Big Data: Privacy and Data Mining')
        self.assertEqual(article.authors, ['Lei Xu', 'Chunxiao Jiang', 'Jian Wang', 'Jian Yuan', 'Yong Ren'])
        self.assertEqual(article.journal, 'Access, IEEE')
        self.assertEqual(article.affiliations,
                         ['Dept. of Electron. Eng., Tsinghua Univ., Beijing, China',
                          'Dept. of Electron. Eng., Tsinghua Univ., Beijing, China',
                          'Dept. of Electron. Eng., Tsinghua Univ., Beijing, China',
                          'Dept. of Electron. Eng., Tsinghua Univ., Beijing, China',
                          'Dept. of Electron. Eng., Tsinghua Univ., Beijing, China'])
        self.assertEqual(article.publisher, 'IEEE')
        self.assertEqual(article.publication_date, '2014')

    def test_is_not_repeating_authors(self):
        response = self.client.post(reverse('retrieval:search'),
                                    {'url': 'http://link.springer.com/article/10.1007/s11084-015-9449-y'},
                                    follow=False)
        response = self.client.post(reverse('retrieval:search'),
                                    {'url': 'http://link.springer.com/article/10.1007/s11084-015-9449-y'},
                                    follow=True)
        article = Article.objects.all()[1]
        self.assertEqual(article.title, 'On the Possibility of Habitable Trojan Planets in Binary Star Systems')
        self.assertEqual(article.authors, ['Richard Schwarz', 'Barbara Funk', 'Ákos Bazsó'])
        self.assertEqual(article.journal, 'Origins of Life and Evolution of Biospheres')
        self.assertEqual(article.affiliations,
                         ['University of Vienna',
                          'University of Vienna',
                          'University of Vienna'])
        self.assertEqual(article.publisher, 'Springer Netherlands')
        self.assertEqual(article.publication_date, '27/06/2015')


class ResultsTest(TestCase):
    def test_results_show_author_and_affiliation(self):
        response = self.client.post(reverse('retrieval:search'),
                                    {'url': 'http://link.springer.com/article/10.1007/s11084-015-9449-y'},
                                    follow=True)

        article = Article.objects.first()
        for affil in article.affiliations:
            self.assertContains(response, affil)

            # def test_results_page_shows_authors(self):
            #     article = Article.objects.create(title='Test Article', authors=['Autor 1', 'Autor 2', 'Autor 3'])
            #     self.assertEqual(Article.objects.count(), 1)
            #
            #     response = self.client.get(reverse('retrieval:results', args=(article.pk,)))
            #     self.assertContains(response, 'Autor 1')
            #     self.assertContains(response, 'Autor 2')
            #     self.assertContains(response, 'Autor 3')
            #
            # def test_results_page_shows_only_authors_from_article(self):
            #     article1 = Article.objects.create(title='Test Article', authors=['Autor 1', 'Autor 2', 'Autor 3'])
            #     article2 = Article()
            #     article2.title = 'Test Article 2'
            #     article2.authors = ['Autor 4', 'Autor 5', 'Autor 6']
            #     article2.save()
            #     article3 = Article()
            #     article3.title = 'Test Article 3'
            #     article3.authors = ['Autor 7', 'Autor 8', 'Autor 9']
            #     article3.save()
            #
            #     response = self.client.get(reverse('retrieval:results', args=(article1.pk,)))
            #     self.assertContains(response, 'Autor 1')
            #     self.assertContains(response, 'Autor 2')
            #     self.assertContains(response, 'Autor 3')
            #     self.assertNotContains(response, 'Autor 4')
            #     self.assertNotContains(response, 'Autor 5')
            #     self.assertNotContains(response, 'Autor 6')
            #     self.assertNotContains(response, 'Autor 7')
            #     self.assertNotContains(response, 'Autor 8')
            #     self.assertNotContains(response, 'Autor 9')
