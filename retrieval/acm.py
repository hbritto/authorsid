# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .htmlParser import HtmlParser


class HtmlParserACM(HtmlParser):
    def __init__(self, soup):
        super(HtmlParserACM, self).__init__(soup)

    def parse(self):
        for tag in self.soup.find_all('meta'):
            self.get_journal_article(tag)
            self.get_publisher_article(tag)
            self.get_publication_date_article(tag)
            self.get_title_article(tag)
            self.get_author_article(tag)
        self.get_affiliation_article()

    def get_author_article(self, tag):
        try:
            if 'citation_authors' in tag['name']:
                authors = tag['content']
                authors = authors.split(';')
                for author in authors:
                    self.authors.append(author.strip())
        except IndexError as ie:
            # print(str(ie))
            pass
        except KeyError as ke:
            # print(str(ke))
            pass

    def get_publication_date_article(self, tag):
        try:
            if 'citation_date' in tag['name']:
                self.publication_date = tag['content']
        except KeyError as ke:
            # print(str(ke))
            pass

    def get_affiliation_article(self, **kwargs):
        tags = self.soup.find_all('small')
        for tag in tags:
            self.affiliations.append(tag.text.strip())
