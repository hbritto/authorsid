# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.gis.db import models
from django.core.urlresolvers import reverse
from django.utils import timezone


class Affiliation(models.Model):
    name = models.TextField('Name')
    location = models.PointField('Location', null=True, geography=True)
    insertion_date = models.DateTimeField('Insertion date', default=timezone.now, null=False)
    aff_valid = models.BooleanField('Affiliation valid', default=False, null=False)

    def __str__(self):
        return self.name


class Author(models.Model):
    name = models.TextField('Name')
    email = models.EmailField('Email', null=True, max_length=1000)
    affiliation = models.ForeignKey(Affiliation, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Article(models.Model):
    url = models.URLField('URL', default='', max_length=1000)
    title = models.TextField('Title', default='')
    authors = models.ManyToManyField(Author)
    journal = models.TextField('Journal', default='', null=True)
    publisher = models.TextField('Publisher', null=True)
    publication_date = models.TextField('Publication date', null=True)

    def __str__(self):
        return str(self.pk) + ' - ' + str(self.title)

    def get_absolute_url(self):
        return reverse('retrieval:results', kwargs={'pk': self.pk})
