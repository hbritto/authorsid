# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url

from . import views

app_name = 'retrieval'
urlpatterns = [
    url(r'^article_input/$', views.ArticleFormView.as_view(), name='article_input'),
    url(r'^batch_input/$', views.ArticleBatchView.as_view(), name='batch_input'),
    url(r'^results/(?P<pk>[0-9]+)/$', views.ResultsSingleView.as_view(), name='results_single'),
    url(r'^map/$', views.map_view, name='map'),
    url(r'^search/$', views.search, name='search'),
    url(r'^batch_handle/$', views.batch_handle, name='batch_handle'),
    url(r'^download_stats/$', views.download_statistics, name='download_statistics'),
    url(r'^download_kml/$', views.download_kml, name='download_kml'),
    url(r'^progress/$', views.progress, name='progress')
]
