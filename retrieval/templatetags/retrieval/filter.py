# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import template

register = template.Library()


@register.filter
def index(_list, i):
    try:
        return _list[int(i)]
    except IndexError as ie:
        return str()


@register.filter
def latitude(point):
    try:
        return point.y
    except AttributeError:
        return 0.0


@register.filter
def longitude(point):
    try:
        return point.x
    except:
        return 0.0
