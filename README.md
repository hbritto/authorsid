# README #

* Project name

    Identificação de Pesquisadores com Foco no Curriculum Lattes e Publicações. 

### How do I get set up? ###

* Summary of set up

    This project uses python 3.4 language and PostgreSQL 9.5.

* Configuration and dependencies

    It's recommended that you create a python3 virtual environment specifically for this project

    This tutorial has been tested in fedora 23 and 24.

    In order to run this project in your computer, you need to have installed the following modules:

    1. BeautifulSoup4
    
        To install the Beautiful Soup module, run the following command:

            pip3 install beautifulsoup4

    2. Lxml
    
        To install the lxml module, run the following commands: 

        For Debian-based distros:

            apt-get install python3-dev libxml2-dev libxslt-dev

        For RedHat-based distros:

            yum install python3-devel libxml2-devel libxslt-devel redhat-rpm-config
        
        And then:
        
            pip3 install lxml
        

    3. Requests
    
        To install the requests module, run the following command:

            pip3 install requests
   
    4. Psycopg2

        To install the psycopg2 module, run the following commands after installing the PostgreSQL database:

        For Debian-based distros:

            apt-get install python3-psycopg2 libpq-dev postgresql postgresql-contrib postgresql-server

        For RedHat-based distros:

            yum install python3-psycopg2 libpqxx-devel postgresql postgresql-contrib postgresql-server
        
        And then:
        
            pip3 install psycopg2

    5. Django
    
        To install the django module, run the following command:

            pip3 install Django
        For more information regarding Django, please refer to the [Django Documentation](https://docs.djangoproject.com/en/dev/)

* Database configuration

    It's recommended that you create a database role specific for this project.
    
    At first, you have to manually create an empty database called "authorsId". After that, go to the file *setings.py* inside the **authorsId** folder and change the *USER* and *PASSWORD* database parameters to fit yours.

    After that, go to the root project folder (the one with the *manage.py* file) and run

       python manage.py migrate

    This command will set up the database for the project.

* Django secret key

    The Django secret key, located in the authorsId/settings.py file, must be filled with an unknown string, for security purposes. For more information regarding this topic, please refer to https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SECRET_KEY

* Google maps Javascript API key

    For the 'maps' feature to be operational, an API Key is needed. To retrieve one, please refer to [Google Maps Javascript API - Get a Key](https://developers.google.com/maps/documentation/javascript/get-api-key#key)

* Running the project

    To run the project, inside the root project folder, run

        python manage.py runserver

    And, in your browser, go to the address `127.0.0.1:8000/` or `localhost:8000`

### Who do I talk to? ###

* Repo owner or admin
    
    Henrique Bonini de Britto Menezes (@hbritto) - henrique.menezes@usp.br