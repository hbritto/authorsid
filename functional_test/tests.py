# This file is part of the authorsID project
#
# authorsID is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait


class NewVisitorTest(StaticLiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        # self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def reach_index_page(self):
        self.browser.get(self.live_server_url)
        wdw = WebDriverWait(self.browser, 5)
        wdw.until(expected_conditions.visibility_of_element_located((By.ID, 'id_title')))

    def get_inputbox(self):
        return self.browser.find_element_by_id('id_url')

    def get_list_from_webelement(self, webelements):
        _list = list()
        for element in webelements:
            _list.append(element.text)
        return _list

    def send_url(self, url):
        self.reach_index_page()
        inputbox = self.get_inputbox()
        inputbox.send_keys(url)
        inputbox.send_keys(Keys.ENTER)

    def test_can_enter_an_IEEE_article_and_retrieve_its_authors(self):
        # Gregson has heard about the new program that retrieves the authors from a given article
        # He goes to its home page and verifies that it has a title and a field for him to input the article URL
        self.reach_index_page()

        header_text = self.browser.find_element_by_id('id_title').text
        self.assertIn('Insira aqui', header_text)

        inputbox = self.get_inputbox()
        self.assertEqual(inputbox.get_attribute('placeholder'), 'Article URL')

        # He inputs an article URL in the input field
        inputbox.send_keys('http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6919256')

        # Then he hits enter and verifies that the page refreshes to a list with all the authors from the given article
        send_button = self.browser.find_element_by_id('id_send')
        send_button.click()

        authors = self.browser.find_elements_by_class_name('author')

        self.assertNotEqual(len(authors), 0)

    def test_can_enter_two_articles_and_get_correct_results(self):
        self.send_url('http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6919256')

        authors_first_webelements = self.browser.find_elements_by_class_name('author')
        authors_first = self.get_list_from_webelement(authors_first_webelements)

        self.send_url('http://link.springer.com/article/10.1007/s11084-015-9449-y')

        authors_second_webelements = self.browser.find_elements_by_class_name('author')
        authors_second = self.get_list_from_webelement(authors_second_webelements)

        self.assertNotEqual(authors_first, authors_second)

    def test_can_enter_same_article_twice_and_get_correct_results(self):
        self.send_url('http://onlinelibrary.wiley.com/doi/10.1002/adhm.201500374/abstract')

        authors_first_webelements = self.browser.find_elements_by_class_name('author')
        authors_first = self.get_list_from_webelement(authors_first_webelements)

        self.send_url('http://onlinelibrary.wiley.com/doi/10.1002/adhm.201500374/abstract')

        authors_second_webelements = self.browser.find_elements_by_class_name('author')
        authors_second = self.get_list_from_webelement(authors_second_webelements)

        self.assertEqual(authors_first, authors_second)

    def test_results_have_a_map(self):
        self.send_url('http://link.springer.com/article/10.1007/s11084-015-9449-y')

        gmap = self.browser.find_element_by_id('map')

        # def test_is_showing_markers_in_map(self):
        #
        #     self.send_url('http://link.springer.com/article/10.1007/s11084-015-9449-y')
        #
        #     map = self.browser.find_element_by_id('googleMap')
